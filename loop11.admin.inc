<?php

/**
 * Implements hook_admin_settings() for module settings configuration.
 */
function loop11_admin_settings_form() {
  $form = array();

  // Global kill switch
  $form['loop11_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable loop11'),
    '#default_value' => variable_get('loop11_enable', 0),
  );

  // Fieldset for key entry
  $form['loop11_keyset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Key'),
    '#collapsible' => TRUE,
  );

  // Text box for key entry
  $form['loop11_keyset']['loop11_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Key'),
    '#default_value' => variable_get('loop11_key', ''),
    '#size' => 41,
    '#maxlength' => 40,
    '#required' => TRUE,
    '#element_validate' => array('_loop11_key_validate'),
  );

  // Fieldset for roles
  $form['loop11_role'] = array(
    '#type' => 'fieldset',
    '#title' => t('Roles'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $role_options = array_map('check_plain', user_roles());
  $form['loop11_role']['loop11_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('User roles'),
    '#default_value' => variable_get('loop11_roles', array()),
    '#options' => $role_options,
    '#description' => t('If none of the roles are selected, all users will be have loop11 JavaScript added to their pages. If a user has any of the roles checked, that user will have the code added to their pages (or not added, depending on the setting above).'),
  );


  // PAGE SPECIFIC VISIBILITY CONFIGURATIONS

  $php_access = user_access('use PHP for tracking visibility');
  $visibility = variable_get('loop11_visibility_pages', 0);
  $pages = variable_get('loop11_pages', LOOP11_PAGES);

  $form['page_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pages'),
    '#collapsible' => TRUE,
  );

  if ($visibility == 2 && !$php_access) {
    $form['page_settings'] = array();
    $form['page_settings']['visibility'] = array('#type' => 'value', '#value' => 2);
    $form['page_settings']['pages'] = array('#type' => 'value', '#value' => $pages);
  }
  else {
    $options = array(
      t('Every page except the listed pages'),
      t('The listed pages only'),
    );
    $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));

    if (module_exists('php') && $php_access) {
      $options[] = t('Pages on which this PHP code returns <code>TRUE</code> (experts only)');
      $title = t('Pages or PHP code');
      $description .= ' ' . t('If the PHP option is chosen, enter PHP code between %php. Note that executing incorrect PHP code can break your Drupal site.', array('%php' => '<?php ?>'));
    }
    else {
      $title = t('Pages');
    }
    $form['page_settings']['loop11_visibility_pages'] = array(
      '#type' => 'radios',
      '#title' => t('Add Loop11 JavaScript to specific pages'),
      '#options' => $options,
      '#default_value' => $visibility,
    );
    $form['page_settings']['loop11_pages'] = array(
      '#type' => 'textarea',
      '#title' => $title,
      '#title_display' => 'invisible',
      '#default_value' => $pages,
      '#description' => $description,
      '#rows' => 10,
    );
  }

  return system_settings_form($form);

}

/**
 * Validate the key entered for access to loop 11 facilities
 */
function _loop11_key_validate($element, &$form_state) {
  if (strlen($element['#value']) < 40) {
    form_error($element, t('The key must be 40 characters long.'));
  }
}
