Description
-----------

This module allows you to add JavaScript code to pages within your site to
enable the use of Loop 11 within it. Loop 11 is a remote usability testing tool
that enables you to test the user-experience of any website and identify
navigational and usability issues.

Features
--------

* Allows use of Loop 11 within the given site
* Facility to allow different keys to be added

Installation
------------

Copy the 'loop11' module directory into your Drupal sites/all/modules directory
as usual.
